let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3
let g:netrw_altv = 1
let g:netrw_winsize = 75

let g:airline_powerline_fonts = 1
let g:airline_theme='luna'

:set number
:set hidden
:set splitright
:set sessionoptions=buffers
:set ignorecase
